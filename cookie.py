from __future__ import print_function
import requests.cookies

# import cookielib



c = [
    {
        "domain": ".shutterstock.com",
        "expirationDate": 1896127200,
        "hostOnly": False,
        "httpOnly": False,
        "name": "__ssid",
        "path": "/",
        "sameSite": "no_restriction",
        "secure": False,
        "session": False,
        "storeId": "0",
        "value": "9606bc02-747b-42ef-b851-4e738e7fd1de",
        "id": 1
    },
    {
        "domain": ".shutterstock.com",
        "expirationDate": 1508849880,
        "hostOnly": False,
        "httpOnly": True,
        "name": "_photo_session_id",
        "path": "/",
        "sameSite": "no_restriction",
        "secure": True,
        "session": False,
        "storeId": "0",
        "value": "68214e1d41c4266955645373481a5a42",
        "id": 2
    },
    {
        "domain": ".shutterstock.com",
        "expirationDate": 1557065880,
        "hostOnly": False,
        "httpOnly": False,
        "name": "_ym_uid",
        "path": "/",
        "sameSite": "no_restriction",
        "secure": False,
        "session": False,
        "storeId": "0",
        "value": "1494857922560381083",
        "id": 3
    },
    {
        "domain": ".shutterstock.com",
        "expirationDate": 1507749900,
        "hostOnly": False,
        "httpOnly": True,
        "name": "accts_contributor",
        "path": "/",
        "sameSite": "no_restriction",
        "secure": False,
        "session": False,
        "storeId": "0",
        "value": "MyStocks",
        "id": 4
    },
    {
        "domain": ".shutterstock.com",
        "expirationDate": 1816433340,
        "hostOnly": False,
        "httpOnly": True,
        "name": "accts_customer",
        "path": "/",
        "sameSite": "no_restriction",
        "secure": False,
        "session": False,
        "storeId": "0",
        "value": "yanushkov",
        "id": 5
    },
    {
        "domain": ".shutterstock.com",
        "expirationDate": 1526393880,
        "hostOnly": False,
        "httpOnly": False,
        "name": "ajs_anonymous_id",
        "path": "/",
        "sameSite": "no_restriction",
        "secure": False,
        "session": False,
        "storeId": "0",
        "value": "%226ae010e6-859b-484c-b5d9-6221581e51cf%22",
        "id": 6
    },
    {
        "domain": ".shutterstock.com",
        "expirationDate": 1536699542,
        "hostOnly": False,
        "httpOnly": False,
        "name": "ajs_group_id",
        "path": "/",
        "sameSite": "no_restriction",
        "secure": False,
        "session": False,
        "storeId": "0",
        "value": "null",
        "id": 7
    },
    {
        "domain": ".shutterstock.com",
        "expirationDate": 1536699542,
        "hostOnly": False,
        "httpOnly": False,
        "name": "ajs_user_id",
        "path": "/",
        "sameSite": "no_restriction",
        "secure": False,
        "session": False,
        "storeId": "0",
        "value": "null",
        "id": 8
    },
    {
        "domain": ".shutterstock.com",
        "expirationDate": 1820517900,
        "hostOnly": False,
        "httpOnly": True,
        "name": "did",
        "path": "/",
        "sameSite": "no_restriction",
        "secure": False,
        "session": False,
        "storeId": "0",
        "value": "a9b91f2e-f3b6-4532-b41e-b34bd7cb67e7",
        "id": 9
    },
    {
        "domain": ".shutterstock.com",
        "expirationDate": 1535966160,
        "hostOnly": False,
        "httpOnly": False,
        "name": "IRF_3",
        "path": "/",
        "sameSite": "no_restriction",
        "secure": False,
        "session": False,
        "storeId": "0",
        "value": "%7Bvisits%3A14%2Cuser%3A%7Btime%3A1494857922704%2Cref%3A%22direct%22%2Cpv%3A39%2Ccap%3A%7B%7D%2Cv%3A%7B%7D%7D%2Cvisit%3A%7Btime%3A1504427803093%2Cref%3A%22https%3A%2F%2Fsubmit.shutterstock.com%2Fearnings%2Fdaily%3Fpage%3D3%26date%3D2017-08-01%26language%3Den%26category%3D25_a_day%26sort%3Ddesc%26sorted_by%3Dcount%26per_page%3D20%22%2Cpv%3A12%2Ccap%3A%7B47%3A1%7D%2Cv%3A%7B1225%3A%22IR_AN_64%22%7D%7D%2Clp%3A%22https%3A%2F%2Fwww.shutterstock.com%2Fru%2Fimage-vector%2Fhand-drawn-botanical-art-isolated-on-687437659%22%2Cdebug%3A0%2Ca%3A1504430191128%2Cd%3A%22shutterstock.com%22%7D",
        "id": 10
    },
    {
        "domain": ".shutterstock.com",
        "hostOnly": False,
        "httpOnly": False,
        "name": "IRMS_la1305",
        "path": "/",
        "sameSite": "no_restriction",
        "secure": False,
        "session": True,
        "storeId": "0",
        "value": "1504427803784",
        "id": 11
    },
    {
        "domain": ".shutterstock.com",
        "expirationDate": 1536699646.424689,
        "hostOnly": False,
        "httpOnly": False,
        "name": "language",
        "path": "/",
        "sameSite": "no_restriction",
        "secure": False,
        "session": False,
        "storeId": "0",
        "value": "en",
        "id": 12
    },
    {
        "domain": ".shutterstock.com",
        "expirationDate": 1535966160,
        "hostOnly": False,
        "httpOnly": False,
        "name": "locale",
        "path": "/",
        "sameSite": "no_restriction",
        "secure": False,
        "session": False,
        "storeId": "0",
        "value": "ru",
        "id": 13
    },
    {
        "domain": ".shutterstock.com",
        "expirationDate": 1819790160,
        "hostOnly": False,
        "httpOnly": False,
        "name": "optimizelyEndUserId",
        "path": "/",
        "sameSite": "no_restriction",
        "secure": False,
        "session": False,
        "storeId": "0",
        "value": "oeu1498041993482r0.3441372721886864",
        "id": 14
    },
    {
        "domain": ".shutterstock.com",
        "expirationDate": 1535966160,
        "hostOnly": False,
        "httpOnly": False,
        "name": "optly.pgStore.p.0",
        "path": "/",
        "sameSite": "no_restriction",
        "secure": False,
        "session": False,
        "storeId": "0",
        "value": "{\"loggedInState\":{\"value\":\"Yes\",\"exp\":1512206190262},\"hasAccount\":{\"value\":\"Yes\",\"exp\":1512206190800}}",
        "id": 15
    },
    {
        "domain": ".shutterstock.com",
        "hostOnly": False,
        "httpOnly": False,
        "name": "optly.pgStore.s.0",
        "path": "/",
        "sameSite": "no_restriction",
        "secure": False,
        "session": True,
        "storeId": "0",
        "value": "{}",
        "id": 16
    },
    {
        "domain": ".shutterstock.com",
        "expirationDate": 1508692620,
        "hostOnly": False,
        "httpOnly": False,
        "name": "psst",
        "path": "/",
        "sameSite": "no_restriction",
        "secure": False,
        "session": False,
        "storeId": "0",
        "value": "115:466,158:572,218:872,332:1160,377:1334,454:1558,530:1787,542:1853,551:1874",
        "id": 17
    },
    {
        "domain": ".shutterstock.com",
        "expirationDate": 1505165364.467738,
        "hostOnly": False,
        "httpOnly": False,
        "name": "visit_id",
        "path": "/",
        "sameSite": "no_restriction",
        "secure": False,
        "session": False,
        "storeId": "0",
        "value": "15831945931",
        "id": 18
    },
    {
        "domain": ".shutterstock.com",
        "expirationDate": 1820696364.468469,
        "hostOnly": False,
        "httpOnly": False,
        "name": "visitor_id",
        "path": "/",
        "sameSite": "no_restriction",
        "secure": False,
        "session": False,
        "storeId": "0",
        "value": "11011468609",
        "id": 19
    },
    {
        "domain": ".submit.shutterstock.com",
        "expirationDate": 1568225520,
        "hostOnly": False,
        "httpOnly": False,
        "name": "__utma",
        "path": "/",
        "sameSite": "no_restriction",
        "secure": False,
        "session": False,
        "storeId": "0",
        "value": "265213581.1507426815.1494857922.1504638242.1505153117.27",
        "id": 20
    },
    {
        "domain": ".submit.shutterstock.com",
        "hostOnly": False,
        "httpOnly": False,
        "name": "__utmc",
        "path": "/",
        "sameSite": "no_restriction",
        "secure": False,
        "session": True,
        "storeId": "0",
        "value": "265213581",
        "id": 21
    },
    {
        "domain": ".submit.shutterstock.com",
        "expirationDate": 1520921520,
        "hostOnly": False,
        "httpOnly": False,
        "name": "__utmz",
        "path": "/",
        "sameSite": "no_restriction",
        "secure": False,
        "session": False,
        "storeId": "0",
        "value": "265213581.1494857970.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)",
        "id": 22
    },
    {
        "domain": ".submit.shutterstock.com",
        "expirationDate": 1530470400,
        "hostOnly": False,
        "httpOnly": False,
        "name": "__uvt",
        "path": "/",
        "sameSite": "no_restriction",
        "secure": False,
        "session": False,
        "storeId": "0",
        "value": "",
        "id": 23
    },
    {
        "domain": ".submit.shutterstock.com",
        "expirationDate": 1557929940,
        "hostOnly": False,
        "httpOnly": False,
        "name": "_ga",
        "path": "/",
        "sameSite": "no_restriction",
        "secure": False,
        "session": False,
        "storeId": "0",
        "value": "GA1.3.1507426815.1494857922",
        "id": 24
    },
    {
        "domain": ".submit.shutterstock.com",
        "expirationDate": 1532623800,
        "hostOnly": False,
        "httpOnly": False,
        "name": "uvts",
        "path": "/",
        "sameSite": "no_restriction",
        "secure": False,
        "session": False,
        "storeId": "0",
        "value": "6DIK18ZF738WyVeK",
        "id": 25
    },
    {
        "domain": "submit.shutterstock.com",
        "expirationDate": 1507230300,
        "hostOnly": True,
        "httpOnly": False,
        "name": "sbsid",
        "path": "/",
        "sameSite": "no_restriction",
        "secure": False,
        "session": False,
        "storeId": "0",
        "value": "a8c044ff1ba8114f12f39f2a6024decb",
        "id": 26
    },
    {
        "domain": "submit.shutterstock.com",
        "expirationDate": 1507755564.468629,
        "hostOnly": True,
        "httpOnly": True,
        "name": "session",
        "path": "/",
        "sameSite": "no_restriction",
        "secure": False,
        "session": False,
        "storeId": "0",
        "value": "s%3APOPXQWqr-nYusEExnUPhmPTJ4q76UUXl.iKOHDwAqW8hCq%2FrmqKYBSW8tXwW0Mz45RwTQPe2XW1o",
        "id": 27
    }
]

# <Biscuit>
#         <option name="date" value="1505164233320" />
#         <option name="domain" value=".submit.shutterstock.com" />
#         <option name="name" value="__utmc" />
#         <option name="path" value="/" />
#         <option name="value" value="265213581" />
#       </Biscuit>

print("{", end='')
for n in c:
    # print("<Biscuit>")
    # # c = n["expirationDate"]
    # # print("<option name=\"date\" value=\"" + (str(n["expirationDate"]) if "expirationDate" in n else "") + "\" />")
    # print("<option name=\"date\" value=\"\"" + " />")
    # print("<option name=\"domain\" value=\"" + str(n["domain"]) + "\"" + " />")
    # print("<option name=\"name\" value=\"" + str(n["name"]) + "\"" + " />")
    # print("<option name=\"path\" value=\"" + str(n["path"]) + "\"" + " />")
    # print("<option name=\"value\" value=\"" + str(n["value"] + "\"") + " />")
    # print("</Biscuit>")
    print("\""+str(n["name"])+"\"", end='')
    print(":", end='')
    print("\""+str(n["value"])+"\"", end='')
    print(", ", end='')
print("}")
